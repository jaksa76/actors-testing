package com.zuehlke.actors.twopc.testableexercise;

import com.zuehlke.actors.twopc.ReliableResource;
import com.zuehlke.actors.twopc.testableactors.*;
import org.junit.Before;
import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

/** This is for Exercise 3 */
public class TwoPCTest {

    private TestingEngine engine;
    private TestingActorReference from;

    @Before
    public void setUp() {
        engine = new TestingEngine();
        engine.setMaxNumStates(10000000);
        engine.setMaxDepth(14);
        engine.setMaxCrashes(1);
        engine.setMaxMessageLosses(1);
        engine.setMaxTimeouts(1);
        from = new TestingActorReference("from");
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testEitherAllCommitOrAllRollback() throws Exception {
        Actor resource1 = new ResourceActor("1", new ReliableResource());
        Actor resource2 = new ResourceActor("2", new ReliableResource());
        Actor coordinator = new CoordinatorActor(asList(resource1, resource2).stream().map(Actor::self).collect(toList()));
        SystemState initial = new SystemState(
            asList(coordinator, resource1, resource2),
            asList(new SystemState.TravellingMessage(new Messages.CoordinatorCommit(), from, coordinator.self()))
        );

        engine.verifyAllStates(initial, this::verify);
    }

    private boolean verify(SystemState state) {
        return false; // TODO verification goes here
    }
}