package com.zuehlke.actors.twopc.testableexercise;

import com.zuehlke.actors.twopc.Resource;
import com.zuehlke.actors.twopc.testableactors.Actor;

import java.io.Serializable;

public class ResourceActor extends Actor {

    private State state = new State();

    /** used for serialization */
    public ResourceActor() { super(null); }

    public ResourceActor(String name) {
        super(name);
    }

    public ResourceActor(String name, Resource resource) {
        super(name);
        this.state.resource = resource;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        // TODO code goes here
    }

    @Override
    public String toString() {
        return "ResourceActor{" + state + '}';
    }

    private static class State implements Serializable {
        Resource resource;
    }
}
