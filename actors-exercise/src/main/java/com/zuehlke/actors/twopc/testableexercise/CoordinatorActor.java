package com.zuehlke.actors.twopc.testableexercise;

import com.zuehlke.actors.twopc.testableactors.Actor;
import com.zuehlke.actors.twopc.testableactors.ActorReference;

import java.io.Serializable;
import java.util.List;

/**
 * Communicates with the resources and performs the two phase commit.
 */
public class CoordinatorActor extends Actor {

    private ActorState state = new ActorState();

    public CoordinatorActor() {
        super("coordinator");
    }

    public CoordinatorActor(List<ActorReference> resourceActors) {
        super("coordinator");
        this.state.resourceActors = resourceActors;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        // TODO code goes here
    }

    private void broadcastToResources(Object msg) {
        state.resourceActors.forEach(r -> tell(r, msg));
    }

    public static class ActorState implements Serializable {
        List<ActorReference> resourceActors;
        ActorReference twoPhaseCommitCoordinator;
    }

    @Override
    public String toString() {
        return "CoordinatorActor{" +
                '}';
    }
}
