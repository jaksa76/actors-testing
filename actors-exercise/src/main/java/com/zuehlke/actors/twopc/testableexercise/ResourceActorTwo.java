package com.zuehlke.actors.twopc.testableexercise;

import com.zuehlke.actors.twopc.RandomResource;

public class ResourceActorTwo extends ResourceActor {

    public ResourceActorTwo() {
        super("2");
    }

    public ResourceActorTwo(RandomResource resource) {
        super("2", resource);
    }
}
