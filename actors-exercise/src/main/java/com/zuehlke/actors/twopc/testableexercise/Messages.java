package com.zuehlke.actors.twopc.testableexercise;

import java.io.Serializable;

public class Messages {

    public static class CoordinatorCommit extends TwoPCMessage {}
    public static class CoordinatorCommitCompleted implements Serializable {
        public final boolean success;

        public CoordinatorCommitCompleted(boolean success) {
            this.success = success;
        }

        @Override
        public String toString() {
            return "CoordinatorCommitCompleted{" +
                    "success=" + success +
                    '}';
        }
    }

    // TODO messages go here

    private static class TwoPCMessage implements Serializable {
        @Override
        public String toString() {
            return getClass().getSimpleName();
        }
    }
}
