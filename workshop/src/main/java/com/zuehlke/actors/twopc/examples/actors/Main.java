package com.zuehlke.actors.twopc.examples.actors;

import com.zuehlke.actors.twopc.testableactors.ActorReference;
import com.zuehlke.actors.twopc.testableactors.Actors;

public class Main {
    public static void main(String[] args) {
        // Actors are instantiated as plain objects
        ActorReference greeterService = new GreeterService().self();

        // just send the message and don't wait for response
        Actors.tell(greeterService, "George");

        // if you want to hear the response use ask
        String response = Actors.ask(greeterService, "George");
        System.out.println(response);
    }
}
