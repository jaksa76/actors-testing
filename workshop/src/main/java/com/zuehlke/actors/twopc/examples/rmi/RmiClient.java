package com.zuehlke.actors.twopc.examples.rmi;

import com.zuehlke.actors.twopc.rmi.RMIHelper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by jvu on 20/05/2016.
 */
public class RmiClient {
    public static void main(String[] args) throws UnknownHostException, RemoteException, NotBoundException {
        // instantiate the RMIHelper with an InetAddress to connect to an existing instance
        RMIHelper rmi = new RMIHelper(InetAddress.getLocalHost());

        // we find the service that we need
        GreeterService server = rmi.find("greeter-service");

        // and just use it as a local POJO
        System.out.println(server.greet("George"));
    }
}
