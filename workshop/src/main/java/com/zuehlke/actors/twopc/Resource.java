package com.zuehlke.actors.twopc;

public interface Resource {
    boolean prepare();
    void commit();
    void rollback();
}
