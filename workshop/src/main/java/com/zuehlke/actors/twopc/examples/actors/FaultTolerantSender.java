package com.zuehlke.actors.twopc.examples.actors;


import com.zuehlke.actors.twopc.testableactors.RecoverableActor;

// If we want our actor restarted we must implement {@link RecoverableActor}.
public class FaultTolerantSender extends RecoverableActor {

    // The recoverable actor MUST have a no args constructor
    protected FaultTolerantSender() {
        super("fault-tolerant");
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        if (o instanceof String) {
            if (o.equals("reaper")) {
                throw new RuntimeException("Here comes the reaper!");
            } else {
                tell(getSender(), "hello " + o);
            }
        }
    }

    // this is invoked after our actor has recovered
    @Override
    protected void recover() {
        // restore any state from persistent storage here
    }
}
