package com.zuehlke.actors.twopc;

import java.io.Serializable;

/**
 * A basic implementation of {@link Resource}.
 */
public class ReliableResource implements Serializable, Resource {
    private String state;

    public ReliableResource() {}

    public boolean prepare() {
        if (state != null) throw new RuntimeException("Cannot prepare, already " + state);
        state = "prepared";
        return true;
    }

    public void rollback() {
        state = "rolled back";
    }

    public void commit() {
        if (!"prepared".equals(state)) throw new RuntimeException("Committing without being prepared!");
        state = "committed";
    }

    public String getState() {
        return state;
    }
}
