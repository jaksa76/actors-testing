package com.zuehlke.actors.twopc.examples.rmi;

import com.zuehlke.actors.twopc.rmi.RMIHelper;

import java.rmi.RemoteException;

/**
 * This is our service accessible from remote clients.
 */
public class RmiServer implements GreeterService {


    public static void main(String[] args) throws RemoteException {
        // Only one process should create the RMIHelper without arguments
        RMIHelper rmiHelper = new RMIHelper();

        rmiHelper.bind("greeter-service", new RmiServer());
    }

    // here is our remote method
    public String greet(String name) {
        System.out.println("got a greeting from " + name);
        return "hello " + name;
    }
}
