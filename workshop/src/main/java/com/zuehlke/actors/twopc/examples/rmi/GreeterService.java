package com.zuehlke.actors.twopc.examples.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

// We need an interface to be presented to the client. It needs to extend remote
public interface GreeterService extends Remote {

    // every method must declare to throw RemoteException
    String greet(String name) throws RemoteException;
}
