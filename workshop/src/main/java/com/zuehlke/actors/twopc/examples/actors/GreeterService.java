package com.zuehlke.actors.twopc.examples.actors;

import com.zuehlke.actors.twopc.testableactors.Actor;


public class GreeterService extends Actor {

    public GreeterService() {
        super("greeter-service");
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        if (o instanceof String) {
            tell(getSender(), "hello " + o);
        }
    }
}
