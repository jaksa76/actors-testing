package com.zuehlke.actors.twopc.examples.actors;

import com.zuehlke.actors.twopc.testableactors.Actor;
import com.zuehlke.actors.twopc.testableactors.ActorReference;
import com.zuehlke.actors.twopc.testableactors.Tick;

/**
 * Alternative implementation that uses alarms
 */
public class DelayedGreeter extends Actor {

    private ActorReference sender;
    private String senderName;

    protected DelayedGreeter(String name) {
        super(name);
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        if (o instanceof String) {
            senderName = (String) o;
            sender = getSender();
            setAlarm(1000);
        } else if (o instanceof Tick) {
            tell(sender, "hello " + senderName);
        }
    }
}
