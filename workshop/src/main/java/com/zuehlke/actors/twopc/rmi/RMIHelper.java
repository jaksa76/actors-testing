package com.zuehlke.actors.twopc.rmi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Permission;
import java.util.HashMap;
import java.util.Map;

public class RMIHelper {

	private static final int JNDI_PORT = 1099;
    private static final Logger log = LoggerFactory.getLogger(RMIHelper.class);
    private final Registry service;
    private final Thread keepAliveThread;
    // we need to keep track of all objects to avoid GC
    private final Map<String, Remote> registeredObjects = new HashMap<>();
    private boolean running = true;

    public RMIHelper() throws RemoteException {
        this.service = createService();

        // this thread is necessary so that the process doesn't end
        keepAliveThread = startKeepaliveThread();
    }

    public RMIHelper(InetAddress address) throws RemoteException {
        this.service = getRemoteServiceOn(address);
        this.keepAliveThread = null;
    }

    private synchronized static Registry createService() throws RemoteException {
        setSecurityManager();
        log.info("Creating rmi registry on localhost" );
        return LocateRegistry.createRegistry(JNDI_PORT);
    }

    private synchronized static Registry getRemoteServiceOn(InetAddress address) throws RemoteException {
        log.info("Found rmi registry on " + address.getHostAddress());
        Registry registry = LocateRegistry.getRegistry(address.getHostAddress());
        return registry;
    }

    private static void setSecurityManager() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager() {
                public void checkPermission(Permission perm) {}
            } );
        }
    }

    public void bind(String name, Remote object) throws RemoteException {
        service.rebind(name, UnicastRemoteObject.exportObject(object, JNDI_PORT));
        log.info("bound " + name);
        registeredObjects.put(name, object); // avoid GC
    }

    public void unbind(String registeredObject) {
        try {
            service.unbind(registeredObject);
        } catch (Exception e1) {}
    }

    public <T extends Remote> T find(String name) throws RemoteException, NotBoundException {
        return (T) service.lookup(name);
    }

    public void shutdown() {
        this.running = false;
        this.keepAliveThread.interrupt();
    }

    private Thread startKeepaliveThread() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        log.info("shutting down");
                        for (String registeredObject : registeredObjects.keySet()) {
                            unbind(registeredObject);
                        }
                    }
                }
            }
        };
        thread.start();
        return thread;
    }

    public static void keepTrying(RemoteThrowingRunnable r) {
        while (true) {
            try {
                r.run();
                return;
            } catch (RemoteException e) {}
        }
    }

    public static <T> T keepTrying(RemoteThrowingSupplier<T> s) {
        while (true) {
            try {
                return s.get();
            } catch (RemoteException e) {}
        }
    }

    public static interface RemoteThrowingSupplier<T> {
        public T get() throws RemoteException;
    }

    public static interface RemoteThrowingRunnable {
        public void run() throws RemoteException;
    }
}
