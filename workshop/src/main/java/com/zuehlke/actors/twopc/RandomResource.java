package com.zuehlke.actors.twopc;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

import static com.google.common.base.Charsets.UTF_8;

/**
 * An implementation of {@link Resource} that randomly succeeds or fails.
 */
public class RandomResource implements Serializable, Resource {

    private static Logger log = LoggerFactory.getLogger(RandomResource.class);

    private final File file;

    public RandomResource() { this(null); }

    public RandomResource(String id) {
        this.file = new File(id + "-resource.txt");
    }

    public boolean prepare() {
        boolean result = Math.random() > .1;
        log.info("preparing " + result);
        if (!result) return false;
        try {
            Files.write("prepared", file, UTF_8);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public void rollback() {
        try {
            log.info("rolling back");
            Files.write("rolled_back", file, UTF_8);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void commit() {
        try {
            log.info("committing");
            Files.write("committed", file, UTF_8);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public Optional<String> getState() {
        try {
            return Optional.of(Files.readFirstLine(file, UTF_8));
        } catch (IOException e) {
        }
        return Optional.empty();
    }
}
