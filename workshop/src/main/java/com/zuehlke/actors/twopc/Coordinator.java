package com.zuehlke.actors.twopc;

/**
 * Coordinator for the two phase twoPhaseCommit.
 */
public interface Coordinator {

    /**
     * Performs the two phase twoPhaseCommit on all of the resources.
     *
     * @return true if the commit was successfull, false if there was a rollback
     */
    boolean twoPhaseCommit();
}
