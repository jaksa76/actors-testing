package com.zuehlke.actors.twopc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * A basic implementation of {@link FailingResource}.
 */
public class FailingResource implements Serializable, Resource {
    private static Logger log = LoggerFactory.getLogger(FailingResource.class);

    public boolean prepare() {
        log.info("preparing ERROR");
        return false;
    }

    public void rollback() {
        log.info("rolling back");
    }

    public void commit() {
        log.info("committing");
    }
}
