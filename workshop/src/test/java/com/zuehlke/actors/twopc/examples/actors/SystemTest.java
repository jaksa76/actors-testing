package com.zuehlke.actors.twopc.examples.actors;

import com.zuehlke.actors.twopc.testableactors.*;
import org.junit.Test;

import java.io.Serializable;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

/**
 * Created by jvu on 20/05/2016.
 */
public class SystemTest {
    @Test
    public void testUsingTestEngine() throws Exception {
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
        TestingEngine testingEngine = new TestingEngine();

        Responder responder = new Responder();
        Client client = new Client(responder.self());

        SystemState initialState = new SystemState(asList(client, responder), emptyList());

        testingEngine.verifyAllStates(initialState, s ->
                !s.isFinalState() ||
                        s.getActors().stream()
                                .filter(a -> a instanceof Client)
                                .map(a -> (Client) a)
                                .allMatch(a -> a.state.gotResponse)
        );
    }

    public static class Responder extends RecoverableActor {
        public Responder() {
            super("responder");
        }

        @Override protected void onReceive(Object o) throws Exception {
            tell(getSender(), "pong");
        }

        @Override protected void recover() {}
    }

    public static class Client extends RecoverableActor {
        ClientState state = new ClientState();

        public Client() {
            super("client");
        }

        public Client(ActorReference responder) {
            super("client");
            this.state.responder = responder;
            save(state);
            setAlarm(100);
        }

        @Override
        protected void recover() {
            this.state = load();
            if (!state.gotResponse) setAlarm(100);
        }

        @Override
        protected void onReceive(Object o) throws Exception {
            if (o instanceof Tick && !state.gotResponse) {
                tell(state.responder, "ping");
                setAlarm(100);
            } else {
                state.gotResponse = true;
                save(state);
            }
        }

        private static class ClientState implements Serializable {
            ActorReference responder;
            boolean gotResponse = false;
        }
    }
}