package com.zuehlke.actors.twopc.rmisolution;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteResource extends Remote {

    Boolean prepare() throws RemoteException;

    void rollback()  throws RemoteException;

    void commit()  throws RemoteException;
}
