package com.zuehlke.actors.twopc.rmisolution;

import com.zuehlke.actors.twopc.Coordinator;
import com.zuehlke.actors.twopc.rmi.RMIHelper;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static com.zuehlke.actors.twopc.rmi.RMIHelper.keepTrying;

public class RmiCoordinator implements Coordinator {

    private List<RemoteResource> resources = new ArrayList<>();

    public RmiCoordinator(String... resourceNames) throws Exception {
        RMIHelper rmi = new RMIHelper(InetAddress.getLocalHost());
        for (String resourceName : resourceNames) {
            resources.add(rmi.find(resourceName));
        }
    }

    @Override
    public boolean twoPhaseCommit() {
        if (resources.parallelStream().allMatch(r -> keepTrying(r::prepare))) {
            resources.parallelStream().forEach(r -> keepTrying(r::commit));
            return true;
        } else {
            resources.parallelStream().forEach(r -> keepTrying(r::rollback));
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        new RmiCoordinator("1", "2", "3").twoPhaseCommit();
    }
}
