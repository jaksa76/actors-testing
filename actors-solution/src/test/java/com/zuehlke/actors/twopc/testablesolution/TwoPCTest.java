package com.zuehlke.actors.twopc.testablesolution;

import com.zuehlke.actors.twopc.ReliableResource;
import com.zuehlke.actors.twopc.message.Messages;
import com.zuehlke.actors.twopc.testableactors.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.zuehlke.actors.twopc.testablesolution.ResourceActor.ResourceState.COMMITTED;
import static com.zuehlke.actors.twopc.testablesolution.ResourceActor.ResourceState.INACTIVE;
import static com.zuehlke.actors.twopc.testablesolution.ResourceActor.ResourceState.ROLLED_BACK;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class TwoPCTest {

    private TestingEngine engine;
    private TestingActorReference from;

    @Before
    public void setUp() {
        engine = new TestingEngine();
        engine.setMaxNumStates(10000000);
        engine.setMaxDepth(14);
        engine.setMaxCrashes(1);
        engine.setMaxMessageLosses(1);
        engine.setMaxTimeouts(1);
        from = new TestingActorReference("from");
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testEitherAllCommitOrAllRollback() throws Exception {
        Actor resource1 = new ResourceActor("1", new ReliableResource());
        Actor resource2 = new ResourceActor("2", new ReliableResource());
        Actor coordinator = new CoordinatorActor(asList(resource1, resource2).stream().map(Actor::self).collect(toList()));
        SystemState initial = new SystemState(
            asList(coordinator, resource1, resource2),
            asList(new SystemState.TravellingMessage(new Messages.CoordinatorCommit(), from, coordinator.self()))
        );

        engine.verifyAllStates(initial, this::verify);
    }

    private boolean verify(SystemState state) {
        // we don't care about non final states
        if (!state.isFinalState()) return true;

        System.out.println(state);

        // if we drop a message of type CoordinatorCommit lets return true
        CoordinatorActor coordinator = state.getActor(CoordinatorActor.class);
        if (coordinator != null && coordinator.isInactive()) return true;

        // if all statuses are equal to committed or all equal to rolled back then everything is fine!
        List<ResourceActor.ResourceState> statuses = state.getActors(ResourceActor.class)
                .map(ResourceActor::getPhase)
                .collect(toList());

        return statuses.size() == 2 &&
            (statuses.stream().allMatch(s -> s.equals(COMMITTED)) || statuses.stream().allMatch(s -> s.equals(ROLLED_BACK)));
    }
}