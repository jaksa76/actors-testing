package com.zuehlke.actors.twopc.recovertest;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.util.Duration;

import static akka.pattern.Patterns.ask;

/**
 * Created by jvu on 20/05/2016.
 */
public class RecoverTest {
    public static void main(String[] args) throws Exception {
        ActorSystem actorSystem = ActorSystem.create();

        ActorRef supervisor = actorSystem.actorOf(new Props(Supervisor.class));
        System.out.println("created supervisor");

        Future f = ask(supervisor, new Props(A.class), 1000);
        ActorRef a = (ActorRef) Await.result(f, Duration.Inf());
        System.out.println("created actors");

        a.tell("crash");

        a.tell("ping");
    }
}

