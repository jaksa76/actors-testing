package com.zuehlke.actors.twopc.message;

import java.io.Serializable;

public class Messages {

    public static class CoordinatorCommit extends TwoPCMessage {}
    public static class CoordinatorCommitCompleted implements Serializable {
        public final boolean success;

        public CoordinatorCommitCompleted(boolean success) {
            this.success = success;
        }

        @Override
        public String toString() {
            return "CoordinatorCommitCompleted{" +
                    "success=" + success +
                    '}';
        }
    }

    public static class PrepareRequest extends TwoPCMessage {}
    public static class PrepareResponse implements Serializable {
        public final boolean success;

        public PrepareResponse(boolean success) {
            this.success = success;
        }

        @Override
        public String toString() {
            return "PrepareResponse{" +
                    "success=" + success +
                    '}';
        }
    }

    public static class CommitRequest extends TwoPCMessage {}
    public static class CommitResponse extends TwoPCMessage {}
    public static class RollbackRequest extends TwoPCMessage {}
    public static class RollbackResponse extends TwoPCMessage {}

    private static class TwoPCMessage implements Serializable {
        @Override
        public String toString() {
            return getClass().getSimpleName();
        }
    }
}
