package com.zuehlke.actors.twopc.recovertest;

import akka.actor.UntypedActor;

public class A extends UntypedActor {

    @Override
    public void postRestart(Throwable reason) {
        System.out.println("restarted");
    }

    @Override
    public void onReceive(Object o) throws Exception {
        System.out.println("message received");
        if (o.equals("crash")) {
            System.out.println("crashing");
            throw new RuntimeException("crashing");
        }
    }
}
