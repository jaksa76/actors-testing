package com.zuehlke.actors.twopc.akkasolution;

import akka.actor.UntypedActor;
import com.zuehlke.actors.twopc.RandomResource;
import com.zuehlke.actors.twopc.message.Messages;

public class ResourceActor extends UntypedActor {

    private final RandomResource resource;

    public ResourceActor(RandomResource resource) {
        this.resource = resource;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof Messages.PrepareRequest) {
            getSender().tell(new Messages.PrepareResponse(resource.prepare()), self());
        } else if (o instanceof Messages.CommitRequest) {
            resource.commit();
            getSender().tell(new Messages.CommitResponse(), self());
        } else if (o instanceof Messages.RollbackRequest) {
            resource.rollback();
            getSender().tell(new Messages.RollbackResponse(), self());
        }
    }
}
