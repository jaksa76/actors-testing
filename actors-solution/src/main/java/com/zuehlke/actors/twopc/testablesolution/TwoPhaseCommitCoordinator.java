package com.zuehlke.actors.twopc.testablesolution;

import akka.actor.ActorRef;
import akka.dispatch.Future;
import akka.pattern.Patterns;
import akka.util.Duration;
import akka.util.Timeout;
import com.zuehlke.actors.twopc.Coordinator;
import com.zuehlke.actors.twopc.RandomResource;
import com.zuehlke.actors.twopc.message.Messages;
import com.zuehlke.actors.twopc.message.Messages.CoordinatorCommitCompleted;
import com.zuehlke.actors.twopc.testableactors.ActorReference;
import com.zuehlke.actors.twopc.testableactors.AkkaActorReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static akka.dispatch.Await.result;
import static akka.util.Duration.Inf;
import static alexh.Unchecker.uncheckedGet;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class TwoPhaseCommitCoordinator implements Coordinator {

    private static Logger log = LoggerFactory.getLogger(TwoPhaseCommitCoordinator.class);

    private final ActorReference coordinatorActor;

    public TwoPhaseCommitCoordinator(ActorReference coordinatorActor) {
        this.coordinatorActor = coordinatorActor;
    }

    @Override
    public boolean twoPhaseCommit() {
        log.info("2PC initiated by TwoPhaseCommitCoordinator");
        ActorRef coordinatorRef = ((AkkaActorReference) coordinatorActor).getAkkaReference();
        Future<Object> future = Patterns.ask(coordinatorRef, new Messages.CoordinatorCommit(), new Timeout(Duration.parse("1 day")));
        CoordinatorCommitCompleted result = (CoordinatorCommitCompleted) uncheckedGet(() -> result(future, Inf()));
        log.info("2PC {}", result.success ? "completed" : "failed");
        return result.success;
    }

    public static void main(String[] args) throws InterruptedException {
        List<ActorReference> resourceActors = asList("1", "2", "3").stream()
                .map(name -> new ResourceActor(name, new RandomResource(name)).self())
                .collect(toList());

        ActorReference coordinatorActor = new CoordinatorActor(resourceActors).self();

        TwoPhaseCommitCoordinator coordinator = new TwoPhaseCommitCoordinator(coordinatorActor);
        coordinator.twoPhaseCommit();
    }
}
