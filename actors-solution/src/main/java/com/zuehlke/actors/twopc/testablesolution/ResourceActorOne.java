package com.zuehlke.actors.twopc.testablesolution;

import com.zuehlke.actors.twopc.RandomResource;

public class ResourceActorOne extends ResourceActor {

    public ResourceActorOne() {
        super("1");
    }

    public ResourceActorOne(RandomResource resource) {
        super("1", resource);
    }
}
