package com.zuehlke.actors.twopc.recovertest;

import akka.actor.*;
import akka.util.Duration;

import static akka.actor.SupervisorStrategy.restart;

/**
 * Created by jvu on 20/05/2016.
 */
public class Supervisor extends UntypedActor {
    private SupervisorStrategy strategy = new OneForOneStrategy(10, Duration.apply("1 minute"), t -> restart());

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof Props) {
            Props props = (Props) o;
            ActorRef actor = context().actorOf(props);
            System.out.println("supervisor created actors");
            getSender().tell(actor, self());
        }
    }
}
