package com.zuehlke.actors.twopc.testablesolution;

import com.zuehlke.actors.twopc.message.Messages;
import com.zuehlke.actors.twopc.testableactors.ActorReference;
import com.zuehlke.actors.twopc.testableactors.RecoverableActor;
import com.zuehlke.actors.twopc.testableactors.Tick;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.zuehlke.actors.twopc.testablesolution.CoordinatorActor.CoordinatorPhase.*;

public class CoordinatorActor extends RecoverableActor {

    private static Logger log = LoggerFactory.getLogger(CoordinatorActor.class);

    enum CoordinatorPhase {INACTIVE, PREPARING, COMMITTING, ROLLING_BACK, FINISHED}

    private Set<ActorReference> preparedResources = new HashSet<>();
    private Set<ActorReference> committedResources = new HashSet<>();
    private Set<ActorReference> rolledBackResources = new HashSet<>();

    private ActorState state = new ActorState();

    private static final long RETRY_TIMEOUT = 100;

    public CoordinatorActor() {
        super("coordinator");
    }

    public CoordinatorActor(List<ActorReference> resourceActors) {
        super("coordinator");
        this.state.resourceActors = resourceActors;
        this.state.phase = INACTIVE;
        save(state);
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof Messages.CoordinatorCommit) {
            log.trace("{} -> Received Messages.CoordinatorCommit -> Broadcasting Messages.PrepareRequest", state.phase);
            state.twoPhaseCommitCoordinator = getSender();
            broadcastToResources(new Messages.PrepareRequest());
            state.phase = PREPARING;
            save(state);
            setAlarm(RETRY_TIMEOUT);
        } else if (o instanceof Messages.PrepareResponse && state.phase.equals(PREPARING)) {
            if (((Messages.PrepareResponse) o).success) {
                log.trace("{} -> Received Messages.PrepareResponse == success", state.phase);
                onCompleted(preparedResources, () -> {
                    state.phase = COMMITTING;
                    save(state);
                    broadcastToResources(new Messages.CommitRequest());
                });
            } else {
                log.trace("{} -> Received Messages.PrepareResponse == failure", state.phase);
                state.phase = ROLLING_BACK;
                save(state);
                broadcastToResources(new Messages.RollbackRequest());
            }
        } else if (o instanceof Messages.CommitResponse && state.phase.equals(COMMITTING)) {
            log.trace("{} -> Received Messages.CommitResponse", state.phase);
            onCompleted(committedResources, () -> {
                state.phase = CoordinatorPhase.FINISHED;
                save(state);
                tell(state.twoPhaseCommitCoordinator, new Messages.CoordinatorCommitCompleted(true));
            });
        } else if (o instanceof Messages.RollbackResponse && state.phase.equals(ROLLING_BACK)) {
            log.trace("{} -> Received Messages.RollbackResponse", state.phase);
            onCompleted(rolledBackResources, () -> {
                state.phase = CoordinatorPhase.FINISHED;
                save(state);
                tell(state.twoPhaseCommitCoordinator, new Messages.CoordinatorCommitCompleted(false));
            });
        } else if (o instanceof Tick && !state.phase.equals(FINISHED)) {
            log.trace("{} -> Received Tick -> Broadcasting again the prepare", state.phase);
            switch (state.phase) {
                case PREPARING:
                    broadcastToResources(new Messages.PrepareRequest());
                    break;
                case COMMITTING:
                    broadcastToResources(new Messages.CommitRequest());
                    break;
                case ROLLING_BACK:
                    broadcastToResources(new Messages.RollbackRequest());
                    break;
            }
            setAlarm(RETRY_TIMEOUT);
        }
    }

    @Override
    protected void recover() {
        state = load();
        log.trace("Recovered resource {}", self().getName());
    }

    private void broadcastToResources(Object msg) {
        log.trace("Broadcasting " + msg);
        state.resourceActors.forEach(r -> tell(r, msg));
    }

    private <T> void onCompleted(Set<ActorReference> actorsReplied, Runnable f) {
        actorsReplied.add(getSender());
        if (actorsReplied.containsAll(state.resourceActors)) {
            log.trace("{} -> Completed phase", state.phase);
            f.run();
        }
    }

    public boolean isInactive() {
        return state.phase.equals(CoordinatorPhase.INACTIVE);
    }

    public static class ActorState implements Serializable {
        List<ActorReference> resourceActors;
        CoordinatorPhase phase;
        ActorReference twoPhaseCommitCoordinator;
    }

    @Override
    public String toString() {
        return "CoordinatorActor{" +
                "phase=" + state.phase +
                '}';
    }
}
