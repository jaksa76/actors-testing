package com.zuehlke.actors.twopc.testablesolution;

import com.zuehlke.actors.twopc.Resource;
import com.zuehlke.actors.twopc.message.Messages;
import com.zuehlke.actors.twopc.testableactors.RecoverableActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import static com.zuehlke.actors.twopc.testablesolution.ResourceActor.ResourceState.*;

public class ResourceActor extends RecoverableActor {

    private static Logger log = LoggerFactory.getLogger(ResourceActor.class);

    private State state = new State();

    public enum ResourceState {INACTIVE, PREPARED, COMMITTED, ROLLED_BACK;}

    /** used for serialization */
    public ResourceActor() { super(null); }

    public ResourceActor(String name) {
        super(name);
    }

    public ResourceActor(String name, Resource resource) {
        super(name);
        this.state.resource = resource;
        this.state.phase = INACTIVE;
        save(state);
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof Messages.PrepareRequest) {
            if (state.phase == INACTIVE) {
                boolean resourcePrepared = state.resource.prepare();
                if (resourcePrepared) state.phase = PREPARED;
                save(state);
                tell(getSender(), new Messages.PrepareResponse(resourcePrepared));
            } else if (state.phase == PREPARED || state.phase == COMMITTED) {
                tell(getSender(), new Messages.PrepareResponse(true));
            } else if (state.phase == ROLLED_BACK) {
                tell(getSender(), new Messages.PrepareResponse(false));
            }
        } else if (o instanceof Messages.CommitRequest) {
            if (state.phase != COMMITTED) {
                state.resource.commit();
                state.phase = COMMITTED;
                save(state);
                tell(getSender(), new Messages.CommitResponse());
            }
        } else if (o instanceof Messages.RollbackRequest) {
            if (state.phase != ROLLED_BACK) {
                state.resource.rollback();
                state.phase = ROLLED_BACK;
                save(state);
                tell(getSender(), new Messages.RollbackResponse());
            }
        }
    }

    @Override
    protected void recover() {
        this.state = load();
    }

    public ResourceState getPhase() {
        return state.phase;
    }

    @Override
    public String toString() {
        return "ResourceActor{" + state.phase + '}';
    }

    private static class State implements Serializable {
        Resource resource;
        ResourceState phase;
    }
}
