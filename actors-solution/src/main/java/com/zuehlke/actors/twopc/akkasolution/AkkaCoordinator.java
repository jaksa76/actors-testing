package com.zuehlke.actors.twopc.akkasolution;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.Future;
import akka.util.Timeout;
import com.zuehlke.actors.twopc.Coordinator;
import com.zuehlke.actors.twopc.RandomResource;
import com.zuehlke.actors.twopc.message.Messages;
import com.zuehlke.actors.twopc.message.Messages.CoordinatorCommit;
import com.zuehlke.actors.twopc.message.Messages.CoordinatorCommitCompleted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static akka.dispatch.Await.result;
import static akka.pattern.Patterns.ask;
import static akka.util.Duration.Inf;
import static akka.util.Duration.parse;
import static alexh.Unchecker.uncheckedGet;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class AkkaCoordinator implements Coordinator {

    private static Logger log = LoggerFactory.getLogger(AkkaCoordinator.class);

    private final ActorRef coordinatorActor;

    public AkkaCoordinator(ActorRef coordinatorActor) {
        this.coordinatorActor = coordinatorActor;
    }

    @Override
    public boolean twoPhaseCommit() {
        Future<Object> future = ask(coordinatorActor, new CoordinatorCommit(), new Timeout(parse("1 day")));
        CoordinatorCommitCompleted result = (CoordinatorCommitCompleted) uncheckedGet(() -> result(future, Inf()));
        log.info("2PC " + (result.success ? "completed" : "failed"));
        return result.success;
    }

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("2PC");

        List<ActorRef> resourceActors = asList("1", "2", "3").stream()
                .map(name -> system.actorOf(new Props(() -> new ResourceActor(new RandomResource(name))), name))
                .collect(toList());

        ActorRef coordinatorActor = system.actorOf(new Props(() -> new CoordinatorActor(resourceActors)), "coordinator");

        AkkaCoordinator coordinator = new AkkaCoordinator(coordinatorActor);
        coordinator.twoPhaseCommit();
    }

    private static class CoordinatorActor extends UntypedActor {

        private final List<ActorRef> resourceActors;
        private final Set<ActorRef> preparedResources = new HashSet<>();
        private final Set<ActorRef> committedResources = new HashSet<>();
        private final Set<ActorRef> rolledbackResources = new HashSet<>();
        private ActorRef akkaCoordinator;

        private CoordinatorActor(List<ActorRef> resourceActors) {
            this.resourceActors = resourceActors;
        }

        @Override
        public void onReceive(Object o) throws Exception {
            if (o instanceof CoordinatorCommit) {
                akkaCoordinator = getSender();
                broadcastToResources(new Messages.PrepareRequest());
            } else if (o instanceof Messages.PrepareResponse) {
                if (((Messages.PrepareResponse) o).success) {
                    onCompleted(preparedResources, () -> broadcastToResources(new Messages.CommitRequest()));
                } else {
                    broadcastToResources(new Messages.RollbackRequest());
                }
            } else if (o instanceof Messages.CommitResponse) {
                onCompleted(committedResources, () -> akkaCoordinator.tell(new CoordinatorCommitCompleted(true), self()));
            } else if (o instanceof Messages.RollbackResponse) {
                onCompleted(rolledbackResources, () -> akkaCoordinator.tell(new CoordinatorCommitCompleted(false), self()));
            }
        }

        private void broadcastToResources(Object msg) {
            log.info("broadcasting {}", msg);
            resourceActors.forEach(r -> r.tell(msg, self()));
        }

        private <T> void onCompleted(Set<ActorRef> actorsReplied, Runnable f) {
            actorsReplied.add(getSender());
            if (actorsReplied.containsAll(resourceActors)) {
                log.info("completed phase");
                f.run();
            }
        }
    }
}
