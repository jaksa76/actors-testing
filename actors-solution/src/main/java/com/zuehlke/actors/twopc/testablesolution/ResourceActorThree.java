package com.zuehlke.actors.twopc.testablesolution;

import com.zuehlke.actors.twopc.RandomResource;

public class ResourceActorThree extends ResourceActor {

    public ResourceActorThree() {
        super("3");
    }

    public ResourceActorThree(RandomResource resource) {
        super("3", resource);
    }
}
