package com.zuehlke.actors.twopc.rmiexercise;

import com.zuehlke.actors.twopc.Coordinator;
import com.zuehlke.actors.twopc.rmi.RMIHelper;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class RmiCoordinator implements Coordinator {

    private List<RemoteResource> resources = new ArrayList<>();

    public RmiCoordinator(String... resourceNames) throws Exception {
        RMIHelper rmi = new RMIHelper(InetAddress.getLocalHost());
        for (String resourceName : resourceNames) {
            resources.add(rmi.find(resourceName));
        }
    }

    @Override
    public boolean twoPhaseCommit() {
        // TODO: solution goes in here
        return false;
    }

    public static void main(String[] args) throws Exception {
        new RmiCoordinator("1", "2", "3").twoPhaseCommit();
    }
}
