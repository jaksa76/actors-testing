package com.zuehlke.actors.twopc.rmiexercise;

import com.zuehlke.actors.twopc.RandomResource;
import com.zuehlke.actors.twopc.rmi.RMIHelper;

public class ResourceStarter {

    public static void main(String[] args) throws Exception {
        RMIHelper rmiService = new RMIHelper();
        for (String resourceName : new String[] {"1", "2", "3"}){
            rmiService.bind(resourceName, new ResourceWrapper(new RandomResource(resourceName)));
        }
    }
}
