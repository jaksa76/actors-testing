package com.zuehlke.actors.twopc.rmiexercise;

import com.zuehlke.actors.twopc.Resource;

public class ResourceWrapper implements RemoteResource {

    private Resource resource;

    public ResourceWrapper(Resource resource) {
        this.resource = resource;
    }

    @Override
    public Boolean prepare() {
        return resource.prepare();
    }

    @Override
    public void rollback() {
        resource.rollback();
    }

    @Override
    public void commit() {
        resource.commit();
    }
}
