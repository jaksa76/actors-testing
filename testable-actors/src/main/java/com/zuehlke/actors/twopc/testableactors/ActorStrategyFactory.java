package com.zuehlke.actors.twopc.testableactors;

public class ActorStrategyFactory {
    public static final ActorStrategyFactory INSTANCE = new ActorStrategyFactory();
    private boolean testingMode = false;

    public void setTestingMode(boolean testingMode) {
        this.testingMode = testingMode;
    }

    ActorStrategy createStrategy(Actor actor, String name) {
        return testingMode ? new TestingActorStrategy(actor, name) : new AkkaActorStrategy(actor, name);
    }
}
