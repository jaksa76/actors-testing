package com.zuehlke.actors.twopc.testableactors;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;
import java.util.function.Function;

public class KryoUtils {
    private static Kryo kryo = init();

    private static Kryo init() {
        Kryo kryo = new Kryo();
        kryo.register(UUID.class, new UUIDSerializer());
        kryo.register(File.class, new FileSerializer());
        return kryo;
    }

    public static byte[] serialize(Object o) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeClassAndObject(output, o);
        output.close();
        return baos.toByteArray();
    }

    public static Object deserialize(byte[] bytes) {
        Input input = new Input(bytes);
        return kryo.readClassAndObject(input);
    }

    public static <T> T clone(T o) {
        return (T) deserialize(serialize(o));
    }

    public static boolean equals(Serializable o1, Serializable o2) {
        return Arrays.equals(serialize(o1), serialize(o2));
    }

    private static class UUIDSerializer extends Serializer<UUID> {
        @Override
        public UUID read(Kryo kryo, Input input, Class aClass) {
            return UUID.fromString(kryo.readObject(input, String.class));
        }

        @Override
        public void write(Kryo kryo, Output output, UUID uuid) {
            kryo.writeObject(output, uuid.toString());
        }
    }

    private static class FileSerializer extends Serializer<File> {

        @Override
        public void write(Kryo kryo, Output output, File file) {
            kryo.writeObject(output, file.getAbsolutePath());
        }

        @Override
        public File read(Kryo kryo, Input input, Class<File> aClass) {
            return new File(kryo.readObject(input, String.class));
        }
    }

    private <T, S> Serializer<T> serializer(Function<T, S> to, Function<S, T> from, Class<S> clazz) {
        return new Serializer<T>() {
            @Override
            public void write(Kryo kryo, Output output, T t) {
                kryo.writeObject(output, to.apply(t));
            }

            @Override
            public T read(Kryo kryo, Input input, Class<T> aClass) {
                return from.apply(kryo.readObject(input, clazz));
            }
        };
    }
}
