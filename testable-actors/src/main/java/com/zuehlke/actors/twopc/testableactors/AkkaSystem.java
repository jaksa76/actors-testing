package com.zuehlke.actors.twopc.testableactors;

import akka.actor.ActorSystem;

class AkkaSystem {
    public static final ActorSystem INSTANCE = ActorSystem.create();
}
