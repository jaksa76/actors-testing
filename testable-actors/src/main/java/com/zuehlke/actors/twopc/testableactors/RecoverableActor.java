package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

public abstract class RecoverableActor extends Actor {

    public RecoverableActor(String name) {
        super(name);
    }

    @Override
    boolean isRecoverable() {
        return true;
    }

    /**
     * This is invoked on a new instance after recovery
     */
    protected abstract void recover();

    protected void save(Serializable state) {
        getStrategy().save(state);
    }

    protected <T extends Serializable> T load() {
        return getStrategy().load();
    }
}
