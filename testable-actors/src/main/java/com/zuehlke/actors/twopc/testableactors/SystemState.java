package com.zuehlke.actors.twopc.testableactors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SystemState implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(SystemState.class);

    private Map<String, Actor> actors;
    private Map<String, byte[]> serializedActors;
    private Set<TravellingMessage> messages;
    private Set<RecoverableActor> crashedActors;

    /**
     * Only for serialization
     */
    public SystemState() {}

    public SystemState(List<Actor> actors) {
        this(actors, Collections.emptyList());
    }

    public SystemState(List<Actor> actors, List<TravellingMessage> messages) {
        // in order to limit the number of visited states, we will stop any visit of a state previously visited
        // in order to compare the current state with the previously visited state, states must implement the equals method
        // two states should be equal if the state of each actor alive is equal, the list of crashed serializedActors is equal and the set of messages is equal
        // the set of messages must be equal to another if all messages are equal, but each set can contain multiple messages that are equal among eachother, their number must be the same in both sets
        this.actors = actors.stream().collect(Collectors.toMap(a -> a.self().getName(), a -> a));
        this.serializedActors = actors.stream().collect(Collectors.toMap(a -> a.self().getName(), a -> KryoUtils.serialize(a)));
        this.messages = new HashSet<>(messages);
        crashedActors = new HashSet<>();
    }

    public boolean containsActor(ActorReference ref) {
        return serializedActors.containsKey(ref.getName());
    }

    public <T extends Actor> T getActor(Class<T> clazz) {
        return getActors(clazz)
                .findFirst()
                .orElse(null);
    }

    public <T extends Actor> Stream<T> getActors(Class<T> clazz) {
        return actors.values().stream()
                .filter(a -> clazz.isAssignableFrom(a.getClass()))
                .map(a -> (T) a);
    }

    public <T extends Actor> T getActor(T actor) {
        return (T) getActor(actor.self());
    }

    public Actor getActor(ActorReference ref) {
        return getActor(ref.getName());
    }

    public Actor getActor(String name) {
        return actors.get(name);
    }

    public Set<TravellingMessage> getMessages() {
        return messages;
    }

    public void addMessage(TravellingMessage travellingMessage) {
        this.messages.add(travellingMessage);
    }

    public Collection<Actor> getActors() {
        return actors.values();
    }

    private void makeStateReadyForRestoring(RecoverableActor recovered, RecoverableActor crashed) {
        TestingActorStrategy crashedStrategy = (TestingActorStrategy) crashed.getStrategy();
        TestingActorStrategy recoveredStrategy = (TestingActorStrategy) recovered.getStrategy();
        recoveredStrategy.savedState = crashedStrategy.savedState;
    }

    private RecoverableActor newInstance(RecoverableActor previousInstance) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        try {
            Constructor<? extends RecoverableActor> constructor = previousInstance.getClass().getConstructor(String.class);
            return constructor.newInstance(previousInstance.self().getName());
        } catch (NoSuchMethodException e) {
            return previousInstance.getClass().newInstance();
        }
    }

    public Set<RecoverableActor> getCrashedActors() {
        return crashedActors;
    }

    public boolean hasRecoveringActors() {
        return !crashedActors.isEmpty();
    }

    public boolean hasPendingAlarms() {
        return getActors().stream().anyMatch(a -> getStrategy(a).hasAlarms());
    }

    public RecoverableActor getCrashedActor(ActorReference ref) {
        for (RecoverableActor actor : crashedActors) {
            if (actor.self().equals(ref)) return actor;
        }
        return null;
    }

    public boolean isFinalState() {
        return crashedActors.isEmpty() && messages.isEmpty() && !hasPendingAlarms();
    }

    private TestingActorStrategy getStrategy(Actor actor) {
        return (TestingActorStrategy) actor.getStrategy();
    }

    SystemState cloneAndRecover(Actor a) {
        SystemState newState = new SystemState();
        newState.messages = this.messages;
        newState.actors = new HashMap<>(actors);
        newState.serializedActors = new HashMap<>(serializedActors);
        newState.crashedActors = new HashSet<>(crashedActors);

        RecoverableActor newActor = newState.getCrashedActor(a.self()); // get the same actor from the new state
        newState.recover(newActor);
        return newState;
    }

    void recover(RecoverableActor a) {
        crashedActors.remove(a);
        try {
            RecoverableActor recovered = newInstance(a);
            makeStateReadyForRestoring(recovered, a);
            recovered.recover();
            saveActor(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    SystemState cloneAndTriggerAlarm(Actor actor) {
        SystemState newState = new SystemState();
        newState.crashedActors = this.crashedActors;
        newState.serializedActors = new HashMap<>(serializedActors);
        newState.actors = new HashMap<>(actors);
        newState.messages = new HashSet<>(messages);
        Actor receiver = KryoUtils.clone(newState.getActor(actor.self()));
        newState.triggerAlarm(receiver);
        return newState;
    }

    private void triggerAlarm(Actor receiver) {
        try {
            getStrategy(receiver).triggerAlarm();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        getStrategy(receiver).getOutbox().entries().forEach(e -> addMessage(new TravellingMessage(e.getValue(), receiver.self(), e.getKey())));
        saveActor(receiver);
    }

    SystemState cloneAndDeliver(TravellingMessage m) {
        SystemState newState = new SystemState();
        newState.messages = new HashSet<>(this.messages);
        newState.crashedActors = this.crashedActors;
        newState.serializedActors = new HashMap<>(serializedActors);
        newState.actors = new HashMap<>(actors);
        Actor receiver = KryoUtils.clone(newState.getActor(m.to));
        newState.deliver(m, receiver);
        return newState;
    }

    private void deliver(TravellingMessage m, Actor receiver) {
        try {
            getStrategy(receiver).deliver(m.message, m.from);
            messages.remove(m);

            getStrategy(receiver).getOutbox().entries().forEach(e -> addMessage(new TravellingMessage(e.getValue(), m.to, e.getKey())));
            getStrategy(receiver).clearOutbox();
            saveActor(receiver);
        } catch (Exception e) {
            throw new RuntimeException("Failed in state " + this, e);
        }
    }

    SystemState cloneAndDrop(TravellingMessage m) {
        SystemState newState = new SystemState();
        newState.actors = this.actors;
        newState.serializedActors = this.serializedActors;
        newState.messages = new HashSet<>(messages);
        newState.crashedActors = this.crashedActors;
        newState.drop(m);
        return newState;
    }

    private void drop(TravellingMessage m) {
        messages.remove(m);
    }

    SystemState cloneAndCrash(Actor a) {
        SystemState newState = new SystemState();
        newState.actors = new HashMap<>(actors);
        newState.serializedActors = new HashMap<>(serializedActors);
        newState.messages = this.messages;
        newState.crashedActors = new HashSet<>(crashedActors);
        newState.crash(a);
        return newState;
    }

    private void crash(Actor a) {
        String name = a.self().getName();
        actors.remove(name);
        serializedActors.remove(name);
        if (a.isRecoverable()) crashedActors.add((RecoverableActor) a);
    }

    private void saveActor(Actor a) {
        String name = a.self().getName();
        serializedActors.put(name, KryoUtils.serialize(a));
        actors.put(name, a);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemState that = (SystemState) o;

        if (!actorsAreEqual(this.serializedActors, that.serializedActors)) return false;
        if (!messages.equals(that.messages)) return false;
        return crashedActors.equals(that.crashedActors);
    }

    private boolean messagesAreEqual(Set<Serializable> messages1, Set<Serializable> messages2) {
        if (messages1.size() != messages2.size()) return false;
        for (Serializable msg1 : messages1) {
            if (messages2.stream().anyMatch(msg2 -> KryoUtils.equals(msg1, msg2)));
        }
        return true;
    }

    private boolean actorsAreEqual(Map<String, byte[]> actorSet1, Map<String, byte[]> actorSet2) {
        if (actorSet1.size() != actorSet2.size()) return false;
        for (String actorName : actorSet1.keySet()) {
            byte[] actor2 = actorSet2.get(actorName);
            if (actor2 == null) return false;
            byte[] actor1 = actorSet1.get(actorName);
            if (!(actor1 == actor2 || Arrays.equals(actor1, actor2))) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = serializedActors.size();
        result = 31 * result + messages.hashCode();
        result = 31 * result + crashedActors.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SystemState{" + "actors=" + actors + ", messages=" + messages + '}';
    }

    public static class TravellingMessage implements Serializable {
        public final Object message;
        public final ActorReference from;
        public final ActorReference to;

        /**
         * Only for serialization
         */
        public TravellingMessage() {
            message = null;
            from = null;
            to = null;
        }

        public TravellingMessage(Object message, ActorReference from, ActorReference to) {
            this.message = message;
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TravellingMessage)) return false;

            TravellingMessage that = (TravellingMessage) o;

            if (!KryoUtils.equals(this, that)) return false;
            if (!from.equals(that.from)) return false;
            return to.equals(that.to);
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return message + "{" + from + "->" + to + '}';
        }
    }
}
