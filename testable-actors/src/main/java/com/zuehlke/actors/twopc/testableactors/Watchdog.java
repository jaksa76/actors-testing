package com.zuehlke.actors.twopc.testableactors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * This is the watchdog that restarts crashed actors. It is used by the {@link AkkaActorStrategy} when creating
 * new actors.
 */
class Watchdog extends UntypedActor {
    private static ActorRef watchdog;

    static ActorRef getWatchdog() {
        if (watchdog == null) {
            watchdog = AkkaSystem.INSTANCE.actorOf(new Props(() -> new Watchdog()), "testableActorSystemWatchdog");
        }
        return watchdog;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof ActorCreationRequest) {
            ActorCreationRequest req = (ActorCreationRequest) o;
            ActorRef actorRef = getContext().actorOf(req.props, req.name);
            getSender().tell(actorRef, self());
        }
    }

    static class ActorCreationRequest {
        public final Props props;
        public final String name;

        public ActorCreationRequest(Props props, String name) {
            this.props = props;
            this.name = name;
        }
    }
}
