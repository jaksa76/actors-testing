package com.zuehlke.actors.twopc.testableactors;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

class Serializer {

    private static Logger log = LoggerFactory.getLogger(Serializer.class);

    public static void serialize(String objectPath, Serializable toSerialize) {
        try {
            FileOutputStream fout = new FileOutputStream(objectPath);
            SerializationUtils.serialize(toSerialize, fout);
            fout.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("Not able to serialize coordinator actor", e);
        }
    }

    public static <T> T deserialize(String objectPath) {
        try {
            FileInputStream fin = new FileInputStream(objectPath);
            Object object = SerializationUtils.deserialize(fin);
            return (T) object;
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("Not able to recover coordinator actor", e);
        } catch (ClassCastException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("The serialized object is not of the correct type: " + e.getMessage(), e);
        }
    }
}
