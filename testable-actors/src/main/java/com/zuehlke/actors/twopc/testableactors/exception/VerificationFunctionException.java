package com.zuehlke.actors.twopc.testableactors.exception;

public class VerificationFunctionException extends RuntimeException {

    public VerificationFunctionException(String message) {
        super(message);
    }
}
