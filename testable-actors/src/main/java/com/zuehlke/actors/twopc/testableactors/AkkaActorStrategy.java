package com.zuehlke.actors.twopc.testableactors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.pattern.Patterns;
import akka.util.Duration;
import alexh.Unchecker;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import static com.zuehlke.actors.twopc.testableactors.Tick.TICK;

/**
 * This is the Akka based implementation of the {@link ActorStrategy}. It will create an underlying akka actor and
 * if needed register it with the {@link Watchdog} service. It will act as a bidirectional link between the
 * {@link Actor} and the underlying akka actor.
 */
class AkkaActorStrategy implements ActorStrategy {
    private final AkkaActorReference self;
    private final ActorRef akkaSelf;
    private final Actor actor;
    private ActorReference sender;

    public AkkaActorStrategy(Actor actor, String name) {
        this.actor = actor;
        ActorRef watchdog = Watchdog.getWatchdog();
        Watchdog.ActorCreationRequest creationRequest = new Watchdog.ActorCreationRequest(new Props(() -> new ActorAdaptor(this)), name);
        Future f = Patterns.<ActorRef>ask(watchdog, creationRequest, 10000);
        this.akkaSelf = Unchecker.uncheckedGet(() -> Await.<ActorRef>result(f, Duration.Inf()));
        this.self = new AkkaActorReference(akkaSelf);
    }

    @Override
    public ActorReference getSender() {
        return sender;
    }

    @Override
    public ActorReference self() {
        return self;
    }

    @Override
    public void tell(ActorReference to, Object msg) {
        ((AkkaActorReference)to).getAkkaReference().tell(msg, akkaSelf);
    }

    void deliver(Object msg, ActorRef sender) throws Exception {
        this.sender = new AkkaActorReference(sender);
        this.actor.onReceive(msg);
    }

    @Override
    public void setAlarm(long delay) {
        AkkaSystem.INSTANCE.scheduler().scheduleOnce(Duration.apply(delay, TimeUnit.MILLISECONDS), akkaSelf, TICK);
    }

    @Override
    public void save(Serializable state) {
        Serializer.serialize(self.getName() + ".bin", state);
    }

    @Override
    public <T extends Serializable> T load() {
        return Serializer.deserialize(self.getName() + ".bin");
    }

    private class ActorAdaptor extends UntypedActor {
        private final AkkaActorStrategy strategy;

        public ActorAdaptor(AkkaActorStrategy strategy) {
            this.strategy = strategy;
        }

        @Override
        public void onReceive(Object o) throws Exception {
            strategy.deliver(o, getSender());
        }
    }
}
