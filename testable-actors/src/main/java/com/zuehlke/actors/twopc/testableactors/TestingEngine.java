package com.zuehlke.actors.twopc.testableactors;

import com.zuehlke.actors.twopc.testableactors.exception.VerificationFunctionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;

public class TestingEngine {

    private static final Logger log = LoggerFactory.getLogger(TestingEngine.class);

    private Set<SystemState> visitedStates = new HashSet<>();
    private int maxDepth = 20;
    private int maxNumStates = 20000;
    private int maxMessageLosses = 1;
    private int maxCrashes = 1;
    private int maxTimeouts = 20;
    private int numStates = 0;
    private boolean verificationFailed = false;
    private boolean maxNumStatesReached = false;
    private boolean hasNotReachedFinalState = true;

    public void verifyAllStates(SystemState initialState, Function<SystemState, Boolean> verificationFunction) {
        visitedStates.clear();
        numStates = 0;
        if (initialState.isFinalState()) log.warn("verifyAllStates(): initial state is already a final system state!");
        constrainedTraverseStatesDFS(initialState, verificationFunction, 0, 0, 0, 0);
        if (verificationFailed) throw new VerificationFunctionException("Verification function failed: for one or more system states");
        if (maxNumStatesReached && hasNotReachedFinalState) throw new VerificationFunctionException("Verification function failed: unable to reach at least one final state");
    }

    public void setMaxCrashes(int maxCrashes) {
        this.maxCrashes = maxCrashes;
    }

    public void setMaxMessageLosses(int maxMessageLosses) {
        this.maxMessageLosses = maxMessageLosses;
    }

    public void setMaxTimeouts(int maxTimeouts) {
        this.maxTimeouts = maxTimeouts;
    }

    private void traverseStatesDFS(SystemState initialState, Function<SystemState, Boolean> verificationFunction, int depth) {
        if (depth == maxDepth || numStates++ > maxNumStates) {
            maxNumStatesReached = true;
            return;
        }

        if(initialState.isFinalState()) return;

        log.info("depth: {}, states: {}", depth, numStates);

        allTransitions(initialState).forEach(t -> {
            if (!visitedStates.contains(t)) {
                visitedStates.add(t);
                if(t.isFinalState()) hasNotReachedFinalState = false;
                checkVerificationFunction(verificationFunction, t);
                traverseStatesDFS(t, verificationFunction, depth + 1);
            }
        });
    }

    private void constrainedTraverseStatesDFS(SystemState initialState, Function<SystemState, Boolean> verificationFunction, int depth, int crashes, int messageLosses, int timeouts) {
        if (depth == maxDepth || numStates++ > maxNumStates) {
            maxNumStatesReached = true;
            return;
        }

        if(initialState.isFinalState()) return;

        if (numStates % 100000 == 0) log.info("states visited: {}", numStates);

        baseTransitions(initialState).forEach(t -> {
            if(t.isFinalState()) hasNotReachedFinalState = false;
            checkVerificationFunction(verificationFunction, t);
            constrainedTraverseStatesDFS(t, verificationFunction, depth + 1, crashes, messageLosses, timeouts);
        });

        if (messageLosses < maxMessageLosses) {
            messageLossTransitions(initialState).forEach(t -> {
                if (t.isFinalState()) hasNotReachedFinalState = false;
                checkVerificationFunction(verificationFunction, t);
                constrainedTraverseStatesDFS(t, verificationFunction, depth + 1, crashes, messageLosses + 1, timeouts);
            });
        }

        if (timeouts < maxTimeouts) {
            timeoutTransitions(initialState).forEach(t -> {
                if (t.isFinalState()) hasNotReachedFinalState = false;
                checkVerificationFunction(verificationFunction, t);
                constrainedTraverseStatesDFS(t, verificationFunction, depth + 1, crashes, messageLosses, timeouts + 1);
            });
        }

        if (crashes < maxCrashes) {
            crashTransitions(initialState).forEach(t -> {
                if (t.isFinalState()) hasNotReachedFinalState = false;
                checkVerificationFunction(verificationFunction, t);
                constrainedTraverseStatesDFS(t, verificationFunction, depth + 1, crashes + 1, messageLosses, timeouts);
            });
        }
    }

    private void checkVerificationFunction(Function<SystemState, Boolean> verificationFunction, SystemState t) {
        if (!verificationFunction.apply(t)) {
            verificationFailed = true;
            log.error("verification failed for " + t);
            throw new VerificationFunctionException("verification failed! exiting and failing fast for now.");
        }
    }

    /** Breadth first search */
    private void traverseStatesBFS(SystemState initialState, Function<SystemState, Boolean> verificationFunction) {
        if(initialState.isFinalState()) return;

        Queue<SystemState> statesToVisit = new LinkedList<>();
        statesToVisit.add(initialState);

        int numStates = 0;
        while (!statesToVisit.isEmpty() && numStates < maxNumStates) {
            SystemState state = statesToVisit.remove();
            if (!visitedStates.contains(state)) {
                visitedStates.add(state);
                log.debug("states: {}", numStates);
                if (state.isFinalState()) hasNotReachedFinalState = false;
                numStates++;
                checkVerificationFunction(verificationFunction, state);
                if (!state.isFinalState())
                    statesToVisit.addAll(allTransitions(state));
            }
        }
        if (numStates == maxNumStates) maxNumStatesReached = true;
    }

    public List<SystemState> allTransitions(SystemState initialState) {
        ArrayList<SystemState> states = new ArrayList<>();
        addBaseTransitions(initialState, states);
        addTimeoutTransitions(initialState, states);
        addMessageLossTransitions(initialState, states);
        addCrashTransitions(initialState, states);
        return states;
    }

    public List<SystemState> baseTransitions(SystemState initialState) {
        ArrayList<SystemState> states = new ArrayList<>();
        addBaseTransitions(initialState, states);
        return states;
    }

    public List<SystemState> messageLossTransitions(SystemState initialState) {
        ArrayList<SystemState> states = new ArrayList<>();
        addMessageLossTransitions(initialState, states);
        return states;
    }

    private ArrayList<SystemState> crashTransitions(SystemState initialState) {
        ArrayList<SystemState> states = new ArrayList<>();
        addCrashTransitions(initialState, states);
        return states;
    }

    private ArrayList<SystemState> timeoutTransitions(SystemState initialState) {
        ArrayList<SystemState> states = new ArrayList<>();
        addTimeoutTransitions(initialState, states);
        return states;
    }

    public void addBaseTransitions(SystemState initialState, ArrayList<SystemState> states) {
        // create all transitions where a message is delivered
        initialState.getMessages().forEach(m -> {
            if (initialState.containsActor(m.to)) states.add(initialState.cloneAndDeliver(m));
        });

        // create all transitions where an actor has recovered
        initialState.getCrashedActors().forEach(a -> states.add(initialState.cloneAndRecover(a)));
    }

    private void addTimeoutTransitions(SystemState initialState, ArrayList<SystemState> states) {
        // create all transitions where an alarm has been triggered
        initialState.getActors().stream()
                .filter(a -> ((TestingActorStrategy) a.getStrategy()).hasAlarms())
                .forEach(actorReference -> states.add(initialState.cloneAndTriggerAlarm(actorReference)));
    }

    private void addMessageLossTransitions(SystemState initialState, ArrayList<SystemState> states) {
        // create all transitions where a message is lost
        initialState.getMessages().forEach(m -> states.add(initialState.cloneAndDrop(m)));
    }

    private void addCrashTransitions(SystemState initialState, ArrayList<SystemState> states) {
        // create all transitions where an actor has crashed
        initialState.getActors().forEach(a -> states.add(initialState.cloneAndCrash(a)));
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public void setMaxNumStates(int maxNumStates) {
        this.maxNumStates = maxNumStates;
    }
}
