package com.zuehlke.actors.twopc.testableactors;

import akka.dispatch.Await;
import akka.dispatch.Future;
import akka.pattern.Patterns;
import akka.util.Duration;
import alexh.Unchecker;

/**
 * Helper class for using actors.
 */
public class Actors {
    public static void tell(ActorReference ref, Object msg) {
        if (ref instanceof AkkaActorReference) {
            AkkaActorReference akkaRef = (AkkaActorReference) ref;
            akkaRef.getAkkaReference().tell(msg);
        } else {
            throw new RuntimeException("Testmode is reserved for use with the TestEngine");
        }
    }

    public static <T> T ask(ActorReference ref, Object msg) {
        if (ref instanceof AkkaActorReference) {
            AkkaActorReference akkaRef = (AkkaActorReference) ref;
            Future ask = Patterns.ask(akkaRef.getAkkaReference(), msg, 1000000);
            return (T) Unchecker.uncheckedGet(() -> Await.result(ask, Duration.Inf()));
        } else {
            throw new RuntimeException("Testmode is reserved for use with the TestEngine");
        }
    }
}
