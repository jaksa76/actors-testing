package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

/**
 * Interface used by {@link Actor} that encapsulates all the functionality. There are two implementations,
 * one for the simulation and the other one using Akka.
 */
interface ActorStrategy extends Serializable {
    ActorReference getSender();

    ActorReference self();

    void tell(ActorReference to, Object msg);

    void setAlarm(long delay);

    void save(Serializable state);

    <T extends Serializable> T load();
}
