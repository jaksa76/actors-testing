package com.zuehlke.actors.twopc.testableactors;

import akka.actor.ActorRef;

public class AkkaActorReference implements ActorReference {

    private ActorRef akkaReference;

    public AkkaActorReference(ActorRef akkaReference) {
        this.akkaReference = akkaReference;
    }

    public ActorRef getAkkaReference() {
        return akkaReference;
    }

    @Override
    public String getName() {
        return akkaReference.path().name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AkkaActorReference that = (AkkaActorReference) o;

        return akkaReference.equals(that.akkaReference);

    }

    @Override
    public int hashCode() {
        return akkaReference.hashCode();
    }
}
