package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

public class Tick implements Serializable {

    public static final Tick TICK = new Tick();

    @Override
    public String toString() {
        return "TICK";
    }
}
