package com.zuehlke.actors.twopc.testableactors;

public class TestingActorReference implements ActorReference {

    private final String path;

    /**
     * Only for serialization
     */
    public TestingActorReference() {
        path = null;
    }

    public TestingActorReference(String path) {
        this.path = path;
    }

    @Override
    public String getName() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestingActorReference that = (TestingActorReference) o;

        return path.equals(that.path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    @Override
    public String toString() {
        return path;
    }
}
