package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

public interface ActorReference extends Serializable {

    String getName();
}
