package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

/**
 * This class must allow the implementor to:
 * - register the strategy under a specific name
 * - receive messages from other actors
 * - get the reference of the current message
 * - send messages to other actors
 * - set a timer (send a delayed message to itself)
 *
 * This class must allow the framework to:
 * - intercept sending of messages
 * - deliver messages to the strategy
 * - intercept timeout requests
 */
public abstract class Actor implements Serializable {

    private final ActorStrategy strategy;

    protected Actor(String name) {
        this.strategy = ActorStrategyFactory.INSTANCE.createStrategy(this, name);
    }

    protected ActorReference getSender() {
        return strategy.getSender();
    }

    public ActorReference self() {
        return strategy.self();
    }

    protected void tell(ActorReference to, Object msg) {
        strategy.tell(to, msg);
    }

    protected void setAlarm(long delay) {
        strategy.setAlarm(delay);
    }

    boolean isRecoverable() {
        return false;
    }

    protected abstract void onReceive(Object o) throws Exception;

    ActorStrategy getStrategy() {
        return this.strategy;
    }
}

