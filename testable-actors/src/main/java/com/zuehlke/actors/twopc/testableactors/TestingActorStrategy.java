package com.zuehlke.actors.twopc.testableactors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.SerializationUtils;

import java.io.Serializable;

/**
 * Created by jvu on 19/05/2016.
 */
class TestingActorStrategy implements ActorStrategy {
    private final ActorReference self;
    private ActorReference sender;
    private Multimap<ActorReference, Object> outbox = ArrayListMultimap.create();
    private Actor actor;
    byte[] savedState;

    private int numberOfPendingAlarms;

    public TestingActorStrategy() {
        this.self = null;
    }

    public TestingActorStrategy(Actor actor, String name) {
        this.actor = actor;
        this.self = new TestingActorReference(name);
        this.numberOfPendingAlarms = 0;
    }

    public static TestingActorStrategy getStrategy(Actor actor) {
        return (TestingActorStrategy) actor.getStrategy();
    }

    public ActorReference getSender() {
        return sender;
    }

    public ActorReference self() {
        return self;
    }

    public void tell(ActorReference to, Object msg) {
        outbox.put(to, msg);
    }

    public void setAlarm(long delay) {
        numberOfPendingAlarms++;
    }

    void decrementAlarms() {
        numberOfPendingAlarms--;
    }

    boolean hasAlarms() {
        return numberOfPendingAlarms > 0;
    }

    /**
     * This is for testing purposes only
     */
    void deliver(Object msg, ActorReference from) throws Exception {
        this.sender = from;
        actor.onReceive(msg);
    }

    void setSender(AkkaActorReference sender) {
        this.sender = sender;
    }

    public Multimap<ActorReference, Object> getOutbox() {
        return outbox;
    }

    public void clearOutbox() {
        this.outbox.clear();
    }

    public void triggerAlarm() throws Exception {
        deliver(Tick.TICK, self);
        decrementAlarms();
    }

    @Override
    public void save(Serializable state) {
        this.savedState = SerializationUtils.serialize(state);
    }

    @Override
    public <T extends Serializable> T load() {
        return (T) SerializationUtils.deserialize(this.savedState);
    }
}
