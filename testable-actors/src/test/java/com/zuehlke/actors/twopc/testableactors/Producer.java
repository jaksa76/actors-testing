package com.zuehlke.actors.twopc.testableactors;

import java.io.IOException;
import java.io.Serializable;

class Producer extends RecoverableActor {
    ProducerState state = new ProducerState();

    public Producer() {
        super("producer");
    }

    public Producer(ActorReference consumer) throws IOException {
        super("producer");
        this.state.consumer = consumer;
        this.state.n = 1;
        save(state);
        setAlarm(100);
    }

    @Override
    protected void recover() {
        this.state = load();
        setAlarm(100);
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        if (o instanceof Consumer.Ack && ((Consumer.Ack)o).n == state.n) {
            state.n++;
            save(state);
        } else if (o instanceof Tick){
            tell(state.consumer, state.n);
            if (state.n <= 3) setAlarm(100);
        }
    }

    @Override
    public String toString() {
        return "Producer{n=" + state.n + '}';
    }

    public static class ProducerState implements Serializable {
        ActorReference consumer;
        Integer n = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Producer producer = (Producer) o;

        return state.n.equals(producer.state.n);
    }

    @Override
    public int hashCode() {
        return state.n.hashCode();
    }
}
