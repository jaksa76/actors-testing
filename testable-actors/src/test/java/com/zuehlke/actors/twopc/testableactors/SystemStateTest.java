 package com.zuehlke.actors.twopc.testableactors;

 import org.junit.Assert;
 import org.junit.Before;
 import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
 import static org.junit.Assert.assertEquals;

 public class SystemStateTest {

    @Before
    public void setUp() {
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testEqualityWhenDeliveringMessage() throws Exception {
        StateChangingActor a = new StateChangingActor();
        SystemState.TravellingMessage msg = msg("msg", a, a);
        SystemState initialState = new SystemState(asList(a), asList(msg));
        SystemState s1 = initialState.cloneAndDeliver(msg);
        SystemState s2 = initialState.cloneAndDeliver(msg);
        Assert.assertTrue(s1.equals(s2));
    }

    @Test
    public void testEqualityWhenLoosingMessage() throws Exception {
        StateChangingActor a = new StateChangingActor();
        SystemState.TravellingMessage msg = msg("msg", a, a);
        SystemState initialState = new SystemState(asList(a), asList(msg));
        SystemState s1 = initialState.cloneAndDrop(msg);
        SystemState s2 = initialState.cloneAndDrop(msg);
        Assert.assertTrue(s1.equals(s2));
    }

    @Test
    public void testEqualityWhenCrashing() throws Exception {
        Consumer consumer = new Consumer();
        Producer producer = new Producer(consumer.self());
        SystemState initialState = new SystemState(asList(producer, consumer), emptyList());
        SystemState s1 = initialState.cloneAndCrash(consumer).cloneAndCrash(producer);
        SystemState s2 = initialState.cloneAndCrash(producer).cloneAndCrash(consumer);
        Assert.assertTrue(s1.equals(s2));
    }

    @Test
    public void testEqualityWhenRecovering() throws Exception {
        Consumer consumer = new Consumer();
        Producer producer = new Producer(consumer.self());
        SystemState initialState = new SystemState(asList(producer, consumer), emptyList());
        SystemState s1 = initialState.cloneAndCrash(consumer).cloneAndRecover(consumer);
        SystemState s2 = initialState.cloneAndCrash(producer).cloneAndRecover(producer);
        Assert.assertTrue(s1.equals(s2));
        Assert.assertTrue(s1.equals(initialState));
        Assert.assertTrue(s2.equals(initialState));
    }

    @Test
    public void testEqualityWhenTriggeringAlarm() throws Exception {
        Consumer consumer = new Consumer();
        Producer producer = new Producer(consumer.self());
        SystemState initialState = new SystemState(asList(producer, consumer), emptyList());
        SystemState s1 = initialState.cloneAndTriggerAlarm(producer);
        SystemState s2 = initialState.cloneAndTriggerAlarm(producer);

        Assert.assertTrue(s1.equals(s2));
    }

    @Test
    public void testLeakingAfterDelivery() throws Exception {
        StateChangingActor a = new StateChangingActor();
        SystemState.TravellingMessage msg = msg("msg", a, a);
        SystemState initialState = new SystemState(asList(a), asList(msg));
        SystemState s1 = initialState.cloneAndDeliver(msg);
        assertEquals(0, initialState.getActor(a).i);
        assertEquals(1, s1.getActor(a).i);
        assertEquals(1, initialState.getMessages().size());
        assertEquals(0, s1.getMessages().size());
    }

     @Test
     public void testLeakingAfterAlarm() throws Exception {
         CountingActor a = new CountingActor();
         SystemState initialState = new SystemState(asList(a));
         SystemState s1 = initialState.cloneAndTriggerAlarm(a);
         assertEquals(0, initialState.getActor(a).n);
     }

     private SystemState.TravellingMessage msg(String msg, Actor from, Actor to) {
        return new SystemState.TravellingMessage(msg, from.self(), to.self());
    }
}