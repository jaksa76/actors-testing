package com.zuehlke.actors.twopc.testableactors;

public class CountingActor extends RecoverableActor {
    public int n;

    public CountingActor() {
        super("selfIncrementer");
        n = 0;
        save(n);
        setAlarm(100);
    }

    @Override
    protected void recover() {
        n = load();
        setAlarm(100);
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        n = n + 1;
        save(n);
        if (n < 10) setAlarm(100);
    }

    @Override
    public String toString() { return "CountingActor{n=" + n + '}'; }
}
