package com.zuehlke.actors.twopc.testableactors;

class ForwardingActor extends Actor {

    private ActorReference forwardTo;

    public ForwardingActor() {
        super("forwarding");
    }

    public ForwardingActor(ActorReference forwardTo) {
        super("forwarding");
        this.forwardTo = forwardTo;
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        tell(forwardTo, o);
    }
}
