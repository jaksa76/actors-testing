package com.zuehlke.actors.twopc.testableactors;

import org.junit.Test;

import java.io.Serializable;
import java.util.Arrays;

import static org.junit.Assert.*;

public class KryoUtilsTest {
    @Test
    public void testSerialization() throws Exception {
        assertEquals("asdf", KryoUtils.deserialize(KryoUtils.serialize("asdf")));
    }

    @Test
    public void testSerializedEquality() throws Exception {
        assertTrue(Arrays.equals(KryoUtils.serialize("asdf"), KryoUtils.serialize("asdf")));
    }

    @Test
    public void testCloning() throws Exception {
        assertEquals("asdf", KryoUtils.clone("asdf"));
    }

    @Test
    public void testEqualityCheck() throws Exception {
        DummyObject o1 = new DummyObject(42);
        DummyObject o2 = new DummyObject(42);
        assertNotEquals(o1, o2);
        assertTrue(KryoUtils.equals(o1, o2));
    }

    private static class DummyObject implements Serializable {
        public int n;

        public DummyObject(int n) {
            this.n = n;
        }
    }
}