package com.zuehlke.actors.twopc.testableactors;

import java.io.Serializable;

class Consumer extends RecoverableActor {
    public static class Ack implements Serializable {
        public Ack(int n) { this.n = n; }
        public int n;
        @Override public String toString() { return "ack"; }
    }

    Integer n = 0;

    public Consumer() {
        super("consumer");
        save(n);
    }

    @Override
    protected void recover() {
        n = load();
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        if (o instanceof Integer) {
            Integer integer = (Integer) o;
            if (!integer.equals(n)) {
                n = integer;
                save(n);
            }
            tell(getSender(), new Ack(n));
        }
    }

    @Override
    public String toString() {
        return "Consumer{" + "n=" + n + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Consumer consumer = (Consumer) o;

        return n.equals(consumer.n);
    }

    @Override
    public int hashCode() {
        return n.hashCode();
    }
}
