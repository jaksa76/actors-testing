package com.zuehlke.actors.twopc.testableactors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static com.zuehlke.actors.twopc.testableactors.TestingActorStrategy.getStrategy;
import static org.assertj.core.api.Assertions.assertThat;

public class ActorTest {

    private ActorReference from;

    @Before
    public void setUp() {
        from = new TestingActorReference("from");
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testReceivingMessage() throws Exception {
        StateChangingActor actor = new StateChangingActor();

        actor.onReceive("baz");

        Assert.assertEquals(1, actor.i);
    }

    @Test
    public void testReplyingToMessage() throws Exception {
        ReplyingActor actor = new ReplyingActor();

        getStrategy(actor).deliver("ping", from);

        assertThat(getStrategy(actor).getOutbox().get(from)).containsExactly("pong");
    }

    @Test
    public void testForwardingMessage() throws Exception {
        ActorReference ultimateDestination = new TestingActorReference("destination");
        ForwardingActor actor = new ForwardingActor(ultimateDestination);
        getStrategy(actor).deliver("hello", from);
        assertThat(getStrategy(actor).getOutbox().get(ultimateDestination)).containsExactly("hello");
    }

    @Test
    public void testAlarm() throws InterruptedException {
        ActorStrategyFactory.INSTANCE.setTestingMode(false);

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        Actor actor = new Actor("alarm") {
            @Override
            protected void onReceive(Object o) throws Exception {
                if(o instanceof Tick) {
                    atomicBoolean.set(true);
                } else {
                    setAlarm(10);
                }
            }
        };

        ((AkkaActorReference) actor.self()).getAkkaReference().tell("hello");

        while(!atomicBoolean.get()) Thread.sleep(2);
    }
}