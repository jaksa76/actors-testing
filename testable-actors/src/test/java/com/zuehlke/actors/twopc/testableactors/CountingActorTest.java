package com.zuehlke.actors.twopc.testableactors;

import org.junit.Before;
import org.junit.Test;

import static java.util.Arrays.asList;

public class CountingActorTest {
    private TestingEngine engine;

    @Before
    public void setUp() {
        engine = new TestingEngine();
        engine.setMaxTimeouts(100);
        engine.setMaxCrashes(10);
        engine.setMaxNumStates(9999999);
        engine.setMaxMessageLosses(2);
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testActorAlwaysTerminatesOn10() throws Exception {
        CountingActor a = new CountingActor();
        SystemState initialState = new SystemState(asList(a));
        engine.verifyAllStates(initialState, s -> !s.isFinalState() || s.getActor(a).n == 10);
    }

    @Test
    public void testManuallyTraversingSystemStates() throws Exception {
        CountingActor a = new CountingActor();
        SystemState initialState = new SystemState(asList(a));
        System.out.println(initialState);
        SystemState s = initialState.cloneAndTriggerAlarm(a);
        System.out.println(s);
        s = s.cloneAndCrash(a);
        System.out.println(s);
        s = s.cloneAndRecover(a);
        System.out.println(s);
        System.out.println(initialState);
    }

}
