package com.zuehlke.actors.twopc.testableactors;

class StateChangingActor extends Actor {

    public int i = 0;

    public StateChangingActor() { super("foo"); }

    public StateChangingActor(String name) { super(name); }

    protected void onReceive(Object o) throws Exception {
        i++;
    }

    @Override
    public String toString() {
        return self().getName() + "{" + "i=" + i + '}';
    }
}
