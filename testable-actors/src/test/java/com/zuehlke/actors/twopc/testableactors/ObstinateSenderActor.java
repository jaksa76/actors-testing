package com.zuehlke.actors.twopc.testableactors;

public class ObstinateSenderActor extends Actor {

    private ActorReference receiver;
    private boolean sending = true;

    public ObstinateSenderActor() { super("ObstinateSenderActor"); }

    public ObstinateSenderActor(ActorReference receiver) {
        super("ObstinateSenderActor");
        this.receiver = receiver;
        setAlarm(100);
    }

    @Override
    protected void onReceive(Object o) {
        if (sending && o instanceof Tick) {
            tell(receiver, "ping");
            setAlarm(100);
        } else if (o.equals("pong")) {
            sending = false;
        }
    }

    @Override
    public String toString() {
        return "ObstinateSenderActor{" +
            "receiver=" + receiver +
            ", sending=" + sending +
            '}';
    }
}
