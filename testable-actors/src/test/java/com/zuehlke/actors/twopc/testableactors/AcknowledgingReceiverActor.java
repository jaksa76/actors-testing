package com.zuehlke.actors.twopc.testableactors;

public class AcknowledgingReceiverActor extends Actor {

    private boolean receivedMessage = false;

    public AcknowledgingReceiverActor() {
        super("AcknowledgingReceiverActor");
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        receivedMessage = true;
        tell(getSender(), "pong");
    }

    public boolean hasReceivedMessage() {
        return receivedMessage;
    }

    @Override
    public String toString() {
        return "AcknowledgingReceiverActor{" +
            "receivedMessage=" + receivedMessage +
            '}';
    }
}
