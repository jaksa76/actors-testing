package com.zuehlke.actors.twopc.testableactors;

class ReplyingActor extends Actor {

    public ReplyingActor() {
        super("ReplyingActor");
    }

    @Override
    protected void onReceive(Object o) throws Exception {
        tell(getSender(), "pong");
    }
}
