package com.zuehlke.actors.twopc.testableactors;

import com.zuehlke.actors.twopc.testableactors.exception.VerificationFunctionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

public class TestingEngineTest {

    private ActorReference from;
    private TestingEngine engine;
    int visitedNumberOfStates = 0;

    @Before
    public void setUp() {
        from = new TestingActorReference("from");
        engine = new TestingEngine();
        ActorStrategyFactory.INSTANCE.setTestingMode(true);
    }

    @Test
    public void testSystemStateTransition() throws Exception {
        StateChangingActor stateChangingActor = new StateChangingActor();

        SystemState initialState = new SystemState(asList(stateChangingActor), asList(new SystemState.TravellingMessage("hello", from, stateChangingActor.self())));

        engine.setMaxMessageLosses(999);
        engine.setMaxCrashes(999);
        List<SystemState> transitions = engine.allTransitions(initialState);

        assertThat(transitions).hasSize(3);
        assertThat(transitions.stream().filter(s -> s.getMessages().size() == 0 && s.getActors().size() == 1 && hasBeenIncremented(s.getActor(StateChangingActor.class)))).hasSize(1);
        assertThat(transitions.stream().filter(s -> s.getMessages().size() == 0 && s.getActors().size() == 1 && !hasBeenIncremented(s.getActor(StateChangingActor.class)))).hasSize(1);
        assertThat(transitions.stream().filter(s -> s.getMessages().size() == 1 && s.getActors().size() == 0)).hasSize(1);
    }

    @Test
    public void testVerifyingActorNeverReaches2() throws Exception {
        StateChangingActor stateChangingActor = new StateChangingActor();
        SystemState initialState = new SystemState(asList(stateChangingActor), asList(new SystemState.TravellingMessage("hello", from, stateChangingActor.self())));
        engine.verifyAllStates(initialState, s -> s.getActors().isEmpty() || s.getActor(stateChangingActor).i < 2);
    }

    @Test
    public void testDelayedMessage() {
        AcknowledgingReceiverActor b = new AcknowledgingReceiverActor();
        ObstinateSenderActor a = new ObstinateSenderActor(b.self());
        SystemState initialState = new SystemState(asList(a, b), emptyList());
        engine.verifyAllStates(initialState, s -> !(s.isFinalState() && s.getActors().size() == 2) || s.getActor(b).hasReceivedMessage());
    }

    @Test
    public void testRecovery() throws Exception {
        CountingActor a = new CountingActor();
        SystemState initialState = new SystemState(asList(a));
        engine.verifyAllStates(initialState, s -> {
            if (s.isFinalState()) System.out.println("FINAL!!!");
            System.out.println(s);
            return !s.isFinalState() || s.getActor(a).n == 10;
        });
    }

    @Test
    public void testNumberOfStatesVisited1() {
        StateChangingActor a1 = new StateChangingActor();
        SystemState initialState = new SystemState(asList(a1), asList(new SystemState.TravellingMessage("a", a1.self(), a1.self())));
        final int[] visitedStates = {0};
        engine.verifyAllStates(initialState, s -> {
            System.out.println(s);
            visitedStates[0]++;
            return true;
        });

        // visited states:
        //  message delivered
        //  message lost
        //  actor crashed
        //  message lost and actor crashed (reachable through 2 paths)
        Assert.assertEquals(4, visitedStates[0]);
    }

    @Test
    public void testNumberOfStatesVisited2() {
        StateChangingActor a1 = new StateChangingActor();
        SystemState initialState = new SystemState(asList(a1), asList(new SystemState.TravellingMessage("a", a1.self(), a1.self())));
        final int[] visitedStates = {0};
        engine.setMaxCrashes(0);
        engine.verifyAllStates(initialState, s -> {
            System.out.println(s);
            visitedStates[0]++;
            return true;
        });

        // visited states:
        //  message delivered
        //  message lost
        Assert.assertEquals(2, visitedStates[0]);
    }

    @Test
    public void testNumberOfStatesVisited3() {
        StateChangingActor a1 = new StateChangingActor();
        SystemState initialState = new SystemState(asList(a1), asList(new SystemState.TravellingMessage("a", a1.self(), a1.self())));
        final int[] visitedStates = {0};
        engine.setMaxCrashes(0);
        engine.setMaxMessageLosses(0);
        engine.verifyAllStates(initialState, s -> {
            System.out.println(s);
            visitedStates[0]++;
            return true;
        });

        // visited states:
        //  message delivered
        Assert.assertEquals(1, visitedStates[0]);
    }

    @Test
    public void testNumberOfStatesVisited4() {
        StateChangingActor a1 = new StateChangingActor("a1");
        StateChangingActor a2 = new StateChangingActor("a2");
        SystemState initialState = new SystemState(asList(a1, a2), asList(msgTo("1", a1), msgTo("2", a2)));
        engine.setMaxMessageLosses(0);
        engine.setMaxCrashes(0);
        engine.verifyAllStates(initialState, this::countVisitedStates);
        Assert.assertEquals(4, visitedNumberOfStates);
    }


    @Test(expected = VerificationFunctionException.class)
    public void shouldThrowExceptionIfVerificationFunctionFails() {
        StateChangingActor stateChangingActor = new StateChangingActor();
        SystemState initialState = new SystemState(asList(stateChangingActor), asList(msg("hello", stateChangingActor, stateChangingActor)));
        engine.verifyAllStates(initialState, s -> false);
    }

    private boolean hasBeenIncremented(Actor testableActor) {
        StateChangingActor actor = (StateChangingActor) testableActor;
        return actor.i == 1;
    }

    private SystemState.TravellingMessage tickFor(Actor actor) {
        return msg(Tick.TICK, actor, actor);
    }

    private SystemState.TravellingMessage msg(Object payload, Actor from, Actor to) {
        return new SystemState.TravellingMessage(payload, from.self(), to.self());
    }

    private SystemState.TravellingMessage msgTo(Object payload, Actor to) {
        return new SystemState.TravellingMessage(payload, from, to.self());
    }

    private boolean countVisitedStates(SystemState s) {
        System.out.println(s);
        visitedNumberOfStates++;
        return true;
    }
}